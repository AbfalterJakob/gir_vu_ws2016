<?php

$it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__ . '/../TREC8all/Adhoc'), RecursiveIteratorIterator::LEAVES_ONLY);
libxml_use_internal_errors(true);

$failed_fixes = [];

foreach ($it as $file) {
  $file_name = $file->getPathName();
  if ($file->isFile() && strtolower(substr($file_name, -3)) != 'xml') {
    $file_contents = file_get_contents($file_name);
    echo "started processing $file_name" . PHP_EOL;

    $xml_document = "<DOCS>$file_contents</DOCS>";
    $doc = simplexml_load_string($xml_document);

    // try n fix it...
    if ($doc === false) {
        $xml_document = preg_replace('/<F\sP=\d+>/', '<F>', $xml_document);
        $xml_document = preg_replace('/<FIG\sID=[a-zA-Z-\d]+>/', '<FIG>', $xml_document);
        $xml_document = preg_replace('/&[a-zA-Z]+;/', '', $xml_document);
        $xml_document = preg_replace('/&/', '', $xml_document);
        $xml_document = preg_replace('/<\d+>/', '', $xml_document);
        $xml_document = preg_replace('/<\/\d+>/', '', $xml_document);

        // test if indeed fixed
        $doc = simplexml_load_string($xml_document);
        if( $doc === false ) {
            echo "[ERROR] Document " . $file_name . " couldn't be fixed!!" . PHP_EOL;
            $failed_fixes[] = $file_name;
        }
    }

    echo '  writing file: ' . $file_name . '.xml' . PHP_EOL;
    file_put_contents($file_name . '.xml', $xml_document);
  }
}

file_put_contents('/vagrant/failedfixes.json', json_encode($failed_fixes));
