#!/bin/bash

if [ ! -d "$1" ]
then
  echo "solr dir not defined or does not exist!"
  exit 1;
fi

if [ ! -f TREC8All/trec_eval.9.0/trec_eval ]
then
  cd TREC8all
  rm -rf trec_eval*
  wget http://trec.nist.gov/trec_eval/trec_eval_latest.tar.gz
  tar xvf trec_eval_latest.tar.gz
  cd trec_eval.9.0
  make
  cd ../..
fi

mkdir -p $1/server/solr/trec8/conf/xslt/
cp TREC8all/solr2trec.xsl $1/server/solr/trec8/conf/xslt/

CONF_DIR=$1/server/solr/trec8/conf
cp cfg_backup/trec8-eval/solrconfig-default.xml $CONF_DIR/solrconfig.xml

declare -a arr=('default' 'lm' 'custom-similarity')
for i in "${arr[@]}"
do
  cp cfg_backup/trec8-eval/schema-${i}.xml $CONF_DIR/schema.xml
  $1/bin/solr restart

  sleep 5
  
  RESULT_FILENAME=your_results_file-${i}
  echo $RESULT_FILENAME
  rm $RESULT_FILENAME
  touch $RESULT_FILENAME

  declare -a terms=(
    '401;foreign%20minorities%2C%20Germany'
    '402;behavioral%20genetics'
    '403;osteoporosis'
    '404;Ireland%2C%20peace%20talks'
    '405;cosmic%20events'
    '406;Parkinson%27s%20disease'
    '407;poaching%2C%20wildlife%20preserves'
    '408;tropical%20storms'
    '409;legal%2C%20Pan%20Am%2C%20103'
    '410;Schengen%20agreement'
    '411;salvaging%2C%20shipwreck%2C%20treasure'
    '412;airport%20security'
    '413;steel%20production'
    '414;Cuba%2C%20sugar%2C%20exports'
    '415;drugs%2C%20Golden%20Triangle'
    '416;Three%20Gorges%20Project'
    '417;creativity'
    '418;quilts%2C%20income'
    '419;recycle%2C%20automobile%20tires'
    '420;carbon%20monoxide%20poisoning'
    '421;industrial%20waste%20disposal'
    '422;art%2C%20stolen%2C%20for'
    '423;Milosevic%2C%20Mirjana%20Markovic'
    '424;suicides'
    '425;counterfeiting%20money'
    '426;law%20enforcement%2C%20dogs'
    '427;UV%20damage%2C%20eyes'
    '428;declining%20birth%20rates'
    '429;Legionnaires%27%20disease'
    '430;killer%20bee%20attacks'
    '431;robotic%20technology'
    '432;profiling%2C%20motorists%2C%20police'
    '433;Greek%2C%20philosophy%2C%20stoicism'
    '434;Estonia%2C%20economy'
    '435;curbing%20population%20growth'
    '436;railway%20accidents'
    '437;deregulation%2C%20gas%2C%20electric'
    '438;tourism%2C%20increase'
    '439;inventions%2C%20scientific%20discoveries'
    '440;child%20labor'
    '441;Lyme%20disease'
    '442;heroic%20acts'
    '443;U.S.%2C%20investment%2C%20Africa'
    '444;supercritical%20flu'
    '445;women%20clergy'
    '446;tourists%2C%20violence'
    '447;Stirling%20engine'
    '448;ship%20losses'
    '449;antibiotics%20ineffectiveness'
    '450;King%20Hussein%2C%20peace'
    )

  for j in "${terms[@]}"
  do
    OIFS=$IFS; IFS=';'; t=($j); IFS=$OIFS
    url="http://localhost:8983/solr/trec8/select?q=${t[1]}&topic=${t[0]}&run=${i}&wt=xslt&tr=solr2trec.xsl&fl=*,score"
    echo $url
    curl $url >> $RESULT_FILENAME
  done

  cd TREC8all
  EVAL_FILENAME=evaluation-${i}
  trec_eval.9.0/trec_eval -q -m official -c qrels.trec8.adhoc.parts1-5 ../$RESULT_FILENAME > $EVAL_FILENAME
  mv $EVAL_FILENAME ..
  cd ..
done
