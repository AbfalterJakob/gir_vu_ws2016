#!/bin/bash

if [ ! -d "$1" ]
then
  echo "solr dir not defined or does not exist!"
  exit 1
fi

mkdir -p custom_similarities/org/apache/solr/search/similarities/
mkdir -p custom_similarities/org/apache/lucene/search/similarities/

cp $1/solr/core/src/java/org/apache/solr/search/similarities/BM25DLNSimilarityFactory.java custom_similarities/org/apache/solr/search/similarities/
cp $1/idea-build/solr/solr-core/classes/java/org/apache/solr/search/similarities/BM25DLNSimilarityFactory.class custom_similarities/org/apache/solr/search/similarities/

cp $1/lucene/core/src/java/org/apache/lucene/search/similarities/BM25DLNSimilarity.java custom_similarities/org/apache/lucene/search/similarities/
cp $1/idea-build/lucene/core/classes/java/org/apache/lucene/search/similarities/BM25DLNSimilarity.class custom_similarities/org/apache/lucene/search/similarities/

cd custom_similarities
jar cf custom_similarities.jar org

cd ..
cp custom_similarities/custom_similarities.jar $1/dist/
