#!/bin/bash

if [ ! -d "$1" ]
then
  echo "solr dir not defined or does not exist!"
  exit 1;
fi

cp cfg_backup/trec8/*.xml $1/server/solr/trec8/conf/
