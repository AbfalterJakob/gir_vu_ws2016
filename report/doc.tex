\documentclass{acmsiggraph}               % final
%\documentclass[review]{acmsiggraph}      % review
%\documentclass[widereview]{acmsiggraph}  % wide-spaced review
%\documentclass[preprint]{acmsiggraph}    % preprint

%% Uncomment one of the four lines above depending on where your paper is
%% in the conference process. ``review'' and ``widereview'' are for review
%% submission, ``preprint'' is for pre-publication, and ``final'' is for
%% the version to be printed.

%% These two line bring in essential packages: ``mathptmx'' for Type 1 
%% typefaces, and ``graphicx'' for inclusion of EPS figures.

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage{multirow}
\usepackage{mathptmx}
\usepackage{graphicx}
\usepackage{url}
\usepackage{listings}
\usepackage{tabulary}

%% use this for zero \parindent and non-zero \parskip, intelligently.

\usepackage{parskip}

%% listings configuration
\lstset{
    numbers=left,
    breaklines=true,
    tabsize=2,
    basicstyle=\ttfamily,
    literate={\ \ }{{\ }}1
}

\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}}

%% If you are submitting a paper to the annual conference, please replace 
%% the value ``0'' below with your OnlineID. If you are not submitting this
%% paper to the annual conference, you may safely leave it at ``0'' -- it 
%% will not be included in the output.

\onlineid{0}

%% need to document this!

\acmformat{print}

%% Paper title.

\title{Exercise 1: Indexing and Retrieval with Solr}

%% Author and Affiliation (single author).

%\author{Georg Ursits\thanks{e-mail: e1025726@student.tuwien.ac.at}\\1025726}

%% Author and Affiliation (multiple authors).

\author{Jakob Abfalter\thanks{e-mail:}\\ 1126889 %
\and Georg Ursits\thanks{e-mail: e1025726@student.tuwien.ac.at} \\ 1025726} %
%\and Martha Stewart\thanks{e-mail:martha.stewart@marthastewart.com}\\ Martha Stewart Enterprises \\ Microsoft Research}

%% Keywords that describe your work.

\keywords{}

%%%%%% START OF THE PAPER %%%%%%

\begin{document}

\teaser{
The aim of this exercise is to familiarize you with the main functionalities of Solr search
engine. The exercise consists of indexing a set of documents, retrieving related documents
to some queries, evaluating results, as well as modifying Solr scoring functions. The exercise
can be done in pairs, but each of the members has to be able to answer questions. The
experiments and results should be explained in a short report, and later presented per group
in an hand-in session.
}

%% The ``\maketitle'' command must be the first command after the
%% ``\begin{document}'' command. It prepares and prints the title block.

\maketitle

%% Abstract section.

%\begin{abstract}
%abstract
%\end{abstract}

%% ACM Computing Review (CR) categories. 
%% See <http://www.acm.org/class/1998/> for details.
%% The ``\CRcat'' command takes four arguments.

%\begin{CRcatlist}
%\CRcat{I.4.1}{Image Processing and Computer Vision}{Digitization and Image Capture}{Imaging geometry};
%\CRcat{I.4.8}{Image Processing and Computer Vision}{Scene Analysis}{Time- varying imagery}
%\end{CRcatlist}

%% The ``\keywordlist'' command prints out the keywords.
%\keywordlist

\section*{Introduction}

%% The ``\copyrightspace'' command must be the first command after the 
%% start of the first section of the body of your paper. It ensures the
%% copyright space is left at the bottom of the first column on the first
%% page of your paper.

%\copyrightspace
In preparation of the upcoming tasks the following components of this exercise have been downloaded:

\begin{itemize}
	\item TREC8 AdHoc dataset for indexing and testing via ground truth data \url{http://trec.nist.gov/data/test_coll.html}
	\item \emph{Solr}\footnote{\url{https://lucene.apache.org/solr/}} 6.3.0 \url{http://www-eu.apache.org/dist/lucene/solr/6.3.0/solr-6.3.0.tgz}
\end{itemize}

After acquiring the basic dataset and tool to work with we setup a \emph{Vagrant}\footnote{Vagrant related details can be found on their product website \url{https://www.vagrantup.com/}} environment for the sake of platform independence and ease of protability.

\section{Creating Index}
\label{sec:creating_index}

The first step we took to familiarize with Solr is the setup of a running server, including the management frontend, and one simple core to populate our index. Before we iterated the steps necessary to achieve those two points we prepared and cleaned the files to import. \\
To prepare the TREC8 files for indexing we read all files recursively and corrected malformed attributes and other hurdles preventing the xml based import of the data due to not being well-formed using a tiny PHP script. The corresponding script is located in \lstinline|<project_root>/util/xml_well_formed_fix.php|.\\
Our choice of importing functionality offered by Solr is the Solr inherent \emph{DataImporthandler}\footnote{\url{https://wiki.apache.org/solr/DataImportHandler}}.

We executed the commans below in sequence to setup a Solr core with the name \emph{trec8} and populated it with cleaned up data:
\begin{enumerate}
	\item \lstinline|$cd <project_root>/util| \\ \lstinline|$php xml_well_formed_fix.php|

	clean up non-wellformed xml data found in the extracted files from TREC8 and save them in the following structure:

	\begin{lstlisting}
<DOCS>
	<DOC>
		<DOCNO>...</DOCNO>
		<HEADER>...</HEADER>
		<TEXT>...</TEXT>
	</DOC>
	<!-- .... -->
</DOCS>
	\end{lstlisting}

	we use the regular expressions below to filter malformed parts and attributes from the original data prior to dumping the contents into \emph{.xml} files:

	\begin{itemize}
  	\item \lstinline|'/<F\sP=\d+>/'|
  	\item \lstinline|'/<FIG\sID=[a-zA-Z-\d]+>/'|
  	\item \lstinline|'/&[a-zA-Z]+;/'|
  	\item \lstinline|'/&/'|
		\item \lstinline|'/<\d+>/'|
		\item \lstinline|'/<\/\d+>/'|
	\end{itemize}

	\item \lstinline|$bin/solr start|\footnote{all Solr related commands are expected to be executed from \lstinline|<project_root>/solr-6.3.0/|}

	simply start the basic Solr skeleton without setting up any collections or cores and make the admin console available via http

	\item \lstinline|$bin/solr create -c trec8|

	create one core named trec8 and setup default configuration files ready to be customized

	\item \lstinline|$cd <project_root>| \\ \lstinline|$cp cfg_backup/trec8/*.xml solr-6.3.0/server/solr/trec8/conf/|

	copy the backups of our earlier already prefabricated configuration files, which resemble the result of all our efforts, into the newly created Solr core to apply our custom \emph{RequestHandlers} and \emph{DataImportHandler}

	\paragraph{solrconfig.xml}; added DataImportHandler lib definition and defined RequestHandler by adding:\\
	\lstinline|<lib dir="${solr.install.dir:../../../..}/dist/" regex="solr-dataimporthandler-.*\.jar" />|
	\begin{lstlisting}
<requestHandler name="/dataimport"
	class="solr.DataImportHandler"
	startup="lazy">
	<lst name="defaults">
		<str name="config">
			data-config.xml
		</str>
	</lst>
</requestHandler>
	\end{lstlisting}

	A further change was reversing back to manually managed \emph{schema.xml} configuration instead of the automatically managed schema variant by adding this setting to our \emph{solrconfig.xml}:

	\begin{lstlisting}
<schemaFactory class="ClassicIndexSchemaFactory"/>
	\end{lstlisting}

	\paragraph{data-config.xml}; define a entity called \lstinline|trec8_simple| that traverses the whole dataset recursively and use a
	\emph{XPathEntityProcessor} that adhers to the previously defined structure

	\begin{lstlisting}
<dataConfig>
  <dataSource type="FileDataSource" encoding="UTF-8"/>
  <document>
      <entity name="pickupdir"
              processor="FileListEntityProcessor"
              rootEntity="false"
              dataSource="null"
              baseDir="/vagrant/TREC8all/Adhoc/"
              recursive="true"
              fileName="^.+\.xml$">
          <entity name="trec8_simple"
                  processor="XPathEntityProcessor"
                  stream="true"
                  url="${pickupdir.fileAbsolutePath}"
                  forEach="/DOCS/DOC">
              <field column="id" xpath="/DOCS/DOC/DOCNO"/>
              <field column="header" xpath="/DOCS/DOC/HEADER"/>
              <field column="text" xpath="/DOCS/DOC/TEXT"/>
          </entity>
      </entity>
  </document>
</dataConfig>
	\end{lstlisting}

	\paragraph{schema.xml}; We obtained this file by sticking to the automatically created \emph{managed-schema} to generate the boilerplate which includes the only two fields we created manually, called \emph{text} of type \emph{general\_text} and \emph{header} of type \emph{general\_text}. The second modification we conduct is the specification of a global similarity to override all field specific similarities like so:

	\begin{lstlisting}
  <similarity class="solr.LMJelinekMercerSimilarityFactory" />
	\end{lstlisting}

	\item go to the \emph{core admin} via the Solr admin console and reload the core \emph{trec8}

	\item call the url \url{http://<ip_solr_is_bound_to>:8983/solr/trec8/dataimport?command=full-import} to start the indexing process
\end{enumerate}

\section{Search}
	\paragraph{topics}; For the search part of this exercise we conducted searches on the topics 401, 407, 415 with the \emph{BM25Similarity} used as a default in Solr and the \emph{LMJelinekMercerSimilarity}.\\
	To change the similarity function used by Solr we added the following code into our \emph{schema.xml}:
	
	\begin{lstlisting}
<similarity class="solr.LMJelinekMercerSimilarityFactory" />
	\end{lstlisting}
	
	Table \ref{search-bm25} shows the requested table of returned documents of the BM25 search and table \ref{serach-lm} shows the requested table of with the LMJelinekMercer similarity. As you can see some queries returned irrelevant documents, which where either non relevant from a semantic point of view or because the document only was relevant for one of the query terms, but not both.
	
\section{Modification of BM25Similarity}
	We decided to implement the modification to the BM25Similarity class described in \emph{When documents are very long, bm25 fails!}. We conducted the following steps.
	\begin{itemize}
	\item We downloaded the Solr 6.3 sources and used the \emph{ant idea} command the be able to open the sources in IntelliJ IDEA
	\item Created new java classes \emph{BM25DLNSimilarity.java} and \emph{BM25DLNSimilarityFactory.java} where we initially copied the code from the defaul t BM25Similarity classes.
	\item In the BM25DLNSimilarity.java class we changed the code of the \emph{score} method as follows:
	\begin{itemize}
		\item Made a check if $c'(q, D) < 0$  and if so returned 0.
		\item Added delta values of $0.5$ to the occurences of $c'(q, D)$ in the equasion. The code for the new score method can be witnessed in figure \ref{mbm23-code}
	\end{itemize}
	This changes have the effect that occurences of query terms get slightly more weighted in very long documents.
	\item Compiled the sources and created a new jar package which was then moved into the \emph{dist} directory in the solr files.
	\item Imported the newly created jar in the \emph{solrconfig.xml} and changed the similarity in the schema.xml to our newly created \emph{BM25DLNSimilarity}
	\end{itemize}
	Table \ref{search-custom} shows the results for the same three queries conducted previously. Comparing with the results of the search conducted with the default BM25 you can see that some documents with longer text where scored slightly higher.

\section{Evaluation Results}

One direct conclusion we draw after looking at \emph{MAP}\footnote{\textbf{M}ean \textbf{A}verage \textbf{P}recision} results of BM25 and BM25M in table \ref{evaluation_results} is the local influence on document ranking of the modification. Due to the averaging aspect of MAP the scores between the BM25 columns are identical because longer documents were preferred but on average they produced the same results.\\
With regards to the difference between \emph{LM} and our BM25 variants the interesting observation is a slightly higher MAP score of LM with three outliers of six to eight percent higher ranked topics:
\begin{itemize}
	\item 405
	\item 423
	\item 433
\end{itemize}
Those relatively \emph{big} outliers justify the highter LM score of $5.4\%$ over the BM25 score of $5.2\%$.

\section{Evaluation Comparison}	

\paragraph{Topic 401} Our results of BM25 match exactly with the results of the validation with regards to the \emph{P@10}\footnote{\textbf{P}recision \textbf{at} \textbf{10}} metric, because we classified no document as relevant. Looking at the P@10 of LM we classified the topic/query FBIS3-19765 as relevant from a contextual and semantic perspective, whereas the evaluation only ranked (most likely) FBIS3-20090 as relevant because there is a close syntactical correlation of the document and the query.

\paragraph{Topic 407} Comparing the evaluation results across BM25 and LM yielding $60\%$ and $70\%$ P@10 accuracy with our subjective human classification of the document relevance to be $100\%$ a big difference is evident. This classification difference across all topics shows the difficulty judging contextual relevance from the perspective of a human being versus a valid and objective evaluation process with regards to the ranking approach.

\paragraph{Topic 415} this comparison shows exactly the same result like topic 407. Apparently our subjective classification and contextual perception of the documents is far less strict and probably less suited for broad and extensive evaluation.

\section{Appendix}

All script below assume that all steps described in section \ref{sec:creating_index} have been successfully iterated and Solr is up and running.

\paragraph{build-similarity-jar.sh} this script prepares the basic package structure needed for our custom similarity, copies our implementation from the Solr sources and assembles a jar, which in the last step is placed in the dist folder of our Solr distribution (downloaded during provisioning).

\paragraph{cp-tre8-config.sh, cp-tre8-custom-similarity-config.sh, cp-tre8-lm-config.sh} these scripts take care of setting up the Solr distribution with the \lstinline|data-config.xml|, \lstinline|schema.xml| and \lstinline|solrconfig.xml| config files in their corresponding \lstinline|cfg_backup| subfolders to configure Solr with LM, BM25 or BM25M respectively. The only step needed to make those changes active is to reload the Solr core.

\paragraph{evaluate.sh} this script automates the download and compilation of the \lstinline|trec_eval| tool, sets up the xslt needed to make the Solr output compliant with the evaluation tool and runs all terms obtained from the trec dataset through solr to obtain the results depicted in table \ref{evaluation_results}.

\begin{table*}[t]
\centering
\caption{BM25 Similarity search}
\label{search-bm25}
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
\multirow{2}{*}{Order} & \multicolumn{2}{c|}{401} & \multicolumn{2}{c|}{407}    & \multicolumn{2}{c|}{415} \\ \cline{2-7} 
                       & docno        & relevant  & docno            & relevant & docno        & relevant  \\ \hline
1                      & FBIS3-18916  & false     & FT941-15416      & true     & FBIS3-60083  & true      \\ \hline
2                      & FBIS4-15890  & false     & FT941-5453       & true     & FBIS3-60025  & true      \\ \hline
3                      & FBIS4-42162  & false     & FBIS3-60235      & true     & FT921-3261   & true      \\ \hline
4                      & FBIS4-32230  & false     & FT933-9176       & true     & FBIS3-21943  & true      \\ \hline
5                      & FBIS3-31802  & false     & FT941-15415      & true     & FBIS3-2893   & true      \\ \hline
6                      & FBIS4-32200  & false     & FT941-1511       & true     & FBIS4-45305  & true      \\ \hline
7                      & FBIS3-30706  & false     & FBIS3-41533      & true     & FBIS4-25105  & true      \\ \hline
8                      & FBIS4-66152  & false     & FT921-6003       & true     & FBIS4-2802   & true      \\ \hline
9                      & FBIS3-8308   & false     & FR941104-1-00055 & true     & FBIS4-21166  & true      \\ \hline
10                     & FBIS4-64811  & false     & FBIS4-67534      & true     & FT922-7496   & false     \\ \hline
Relevant Percentage    & -            &           & -                &          & -            &           \\ \hline
\end{tabular}
\end{table*}

\begin{table*}[t]
\centering
\caption{LMJelinekMercerSimilarity search}
\label{serach-lm}
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
\multirow{2}{*}{Order} & \multicolumn{2}{c|}{401} & \multicolumn{2}{c|}{407}    & \multicolumn{2}{c|}{415}  \\ \cline{2-7} 
                       & docno        & relevant  & docno            & relevant & docno        & relevant   \\ \hline
1                      & FBIS3-18916  & false     & FR941104-1-00045 & true     & FT921-3261   & true       \\ \hline
2                      & FT941-4506   & false     & FT941-15416      & true     & FBIS3-60083  & true       \\ \hline
3                      & FBIS4-42162  & false     & FBIS3-60235      & true     & FBIS3-60025  & true       \\ \hline
4                      & FBIS4-62604  & false     & FT941-5453       & true     & FBIS4-45305  & true       \\ \hline
5                      & FBIS3-16595  & false     & FT941-15415      & true     & FBIS4-25105  & true       \\ \hline
6                      & FBIS3-20090  & false     & FT941-1511       & true     & FBIS3-21943  & true       \\ \hline
7                      & FBIS3-19765  & true      & FT933-9176       & true     & FBIS3-2893   & true       \\ \hline
8                      & FBIS4-15890  & true      & FBIS3-41533      & true     & FBIS4-2802   & true       \\ \hline
9                      & FBIS3-31802  & false     & FR941104-1-00055 & true     & FBIS4-21166  & true       \\ \hline
10                     & FT921-727    & false     & FT921-6003       & true     & FBIS3-60116  & true       \\ \hline
Relevant Percentage    & -            &           & -                &          & -            &            \\ \hline
\end{tabular}
\end{table*}

\begin{figure*}[t]
\centering
\caption{Modified BM25 score function
\label{mbm23-code}}
\begin{lstlisting}
		Override
		public float score(int doc, float freq) {
    		float norm = norms == null ? k1 : cache[(byte)norms.get(doc) & 0xFF];
    		float c_apos = freq / (cache[(byte)norms.get(doc) & 0xFF] / k1);
    		float delta = .5f;
    		return c_apos > 0 ? weightValue * (freq + delta) / (freq + norm + delta) : 0;
		}
\end{lstlisting}
\end{figure*}

\begin{table*}[t]
\centering
\caption{Modified BM25 Similarity search}
\label{search-custom}
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
\multirow{2}{*}{Order} & \multicolumn{2}{c|}{401} & \multicolumn{2}{c|}{407}    & \multicolumn{2}{c|}{415} \\ \cline{2-7} 
                       & docno        & relevant  & docno            & relevant & docno        & relevant  \\ \hline
1                      & FBIS3-18916  & false     & FT941-15416      & true     & FBIS3-60083  & true      \\ \hline
2                      & FBIS4-15890  & false     & FT941-5453       & true     & FBIS3-60025  & true      \\ \hline
3                      & FBIS4-42162  & false     & FBIS3-60235      & true     & FT921-3261   & true      \\ \hline
4                      & FBIS4-32230  & false     & FT933-9176       & true     & FBIS3-21943  & true      \\ \hline
5                      & FBIS3-31802  & false     & FT941-15415      & true     & FBIS3-2893   & true      \\ \hline
6                      & FBIS4-32200  & false     & FT941-1511       & true     & FBIS4-25105  & true      \\ \hline
7                      & FBIS3-30706  & false     & FBIS3-41533      & true     & FBIS4-45305  & true      \\ \hline
8                      & FBIS4-66152  & false     & FT921-6003       & true     & FBIS4-21166  & true      \\ \hline
9                      & FBIS4-64811  & false     & FR941104-1-00055 & true     & FBIS4-2802   & true      \\ \hline
10                     & FBIS3-8308   & false     & FBIS4-67534      & true     & FT922-7496   & false     \\ \hline
Relevant Percentage    & -            &           & -                &          & -            &           \\ \hline
\end{tabular}
\end{table*}

\begin{table*}[t]
\centering
\caption{Evaluation results and comparison of BM25, Language Modeling and the modified BM25 similarity}
\label{evaluation_results}
\begin{tabular}{ C{1.5cm} | C{1.5cm} | C{1.5cm} | C{1.5cm} | C{1.5cm} | C{1.5cm} | C{1.5cm} }
\multirow{2}{*}{ } & \multicolumn{2}{c|}{BM25}  & \multicolumn{2}{c|}{LM}  & \multicolumn{2}{c}{BM25DLN} \\ \cline{2-7}
                   & MAP    & P@10              & MAP    & P@10            & MAP     & P@10              \\ \hline \hline
401                & 0.0000 & 0.0000            & 0.0006 & 0.1000          & 0.0000  & 0.0000            \\ \hline
402                & 0.0021 & 0.1000            & 0.0014 & 0.1000          & 0.0021  & 0.1000            \\ \hline
403                & 0.3714 & 0.8000            & 0.3108 & 0.7000          & 0.3714  & 0.8000            \\ \hline
404                & 0.0000 & 0.0000            & 0.0000 & 0.0000          & 0.0000  & 0.0000            \\ \hline
405                & 0.0105 & 0.2000            & 0.0088 & 0.1000          & 0.0105  & 0.2000            \\ \hline
406                & 0.0385 & 0.1000            & 0.0154 & 0.1000          & 0.0385  & 0.1000            \\ \hline
407                & 0.0846 & 0.6000            & 0.0964 & 0.7000          & 0.0846  & 0.6000            \\ \hline
408                & 0.0678 & 0.8000            & 0.0678 & 0.8000          & 0.0678  & 0.8000            \\ \hline
409                & 0.1818 & 0.4000            & 0.1818 & 0.4000          & 0.1818  & 0.4000            \\ \hline
410                & 0.1538 & 1.0000            & 0.1538 & 1.0000          & 0.1538  & 1.0000            \\ \hline
411                & 0.0278 & 0.2000            & 0.0370 & 0.1000          & 0.0278  & 0.2000            \\ \hline
412                & 0.0693 & 0.9000            & 0.0513 & 0.8000          & 0.0693  & 0.9000            \\ \hline
413                & 0.0072 & 0.1000            & 0.0029 & 0.1000          & 0.0072  & 0.1000            \\ \hline
414                & 0.1190 & 0.6000            & 0.0916 & 0.6000          & 0.1190  & 0.6000            \\ \hline
415                & 0.0480 & 0.7000            & 0.0434 & 0.7000          & 0.0480  & 0.7000            \\ \hline
416                & 0.0845 & 0.4000            & 0.0539 & 0.5000          & 0.0845  & 0.4000            \\ \hline
417                & 0.0473 & 0.5000            & 0.0071 & 0.2000          & 0.0473  & 0.5000            \\ \hline
418                & 0.0349 & 0.5000            & 0.0407 & 0.6000          & 0.0349  & 0.5000            \\ \hline
419                & 0.0000 & 0.0000            & 0.0000 & 0.0000          & 0.0000  & 0.0000            \\ \hline
420                & 0.0061 & 0.1000            & 0.0000 & 0.0000          & 0.0061  & 0.1000            \\ \hline
421                & 0.0064 & 0.2000            & 0.0020 & 0.1000          & 0.0064  & 0.2000            \\ \hline
422                & 0.0658 & 1.0000            & 0.0578 & 0.9000          & 0.0658  & 1.0000            \\ \hline
423                & 0.3206 & 0.7000            & 0.3978 & 0.9000          & 0.3206  & 0.7000            \\ \hline
424                & 0.0000 & 0.0000            & 0.0000 & 0.0000          & 0.0000  & 0.0000            \\ \hline
425                & 0.0306 & 0.6000            & 0.0470 & 0.8000          & 0.0306  & 0.6000            \\ \hline
426                & 0.0099 & 0.2000            & 0.0074 & 0.3000          & 0.0099  & 0.2000            \\ \hline
427                & 0.0500 & 0.4000            & 0.0619 & 0.5000          & 0.0500  & 0.4000            \\ \hline
428                & 0.0258 & 0.4000            & 0.0351 & 0.6000          & 0.0258  & 0.4000            \\ \hline
429                & 0.0684 & 0.3000            & 0.0712 & 0.3000          & 0.0684  & 0.3000            \\ \hline
430                & 0.0000 & 0.0000            & 0.0000 & 0.0000          & 0.0000  & 0.0000            \\ \hline
431                & 0.0136 & 0.3000            & 0.0231 & 0.3000          & 0.0136  & 0.3000            \\ \hline
432                & 0.0000 & 0.0000            & 0.0000 & 0.0000          & 0.0000  & 0.0000            \\ \hline
433                & 0.0000 & 0.0000            & 0.0897 & 0.2000          & 0.0000  & 0.0000            \\ \hline
434                & 0.0228 & 0.9000            & 0.0228 & 0.9000          & 0.0228  & 0.9000            \\ \hline
435                & 0.0120 & 0.2000            & 0.0085 & 0.2000          & 0.0120  & 0.2000            \\ \hline
436                & 0.0025 & 0.2000            & 0.0022 & 0.2000          & 0.0025  & 0.2000            \\ \hline
437                & 0.0000 & 0.0000            & 0.0000 & 0.0000          & 0.0000  & 0.0000            \\ \hline
438                & 0.0214 & 0.5000            & 0.0102 & 0.4000          & 0.0214  & 0.5000            \\ \hline
439                & 0.0009 & 0.1000            & 0.0000 & 0.0000          & 0.0009  & 0.1000            \\ \hline
440                & 0.0058 & 0.2000            & 0.0146 & 0.3000          & 0.0058  & 0.2000            \\ \hline
441                & 0.1765 & 0.3000            & 0.2386 & 0.5000          & 0.1765  & 0.3000            \\ \hline
442                & 0.0000 & 0.0000            & 0.0000 & 0.0000          & 0.0000  & 0.0000            \\ \hline
443                & 0.0109 & 0.3000            & 0.0010 & 0.1000          & 0.0109  & 0.3000            \\ \hline
444                & 0.1618 & 0.3000            & 0.2118 & 0.5000          & 0.1618  & 0.3000            \\ \hline
445                & 0.1086 & 0.7000            & 0.1290 & 0.8000          & 0.1086  & 0.7000            \\ \hline
446                & 0.0394 & 0.7000            & 0.0241 & 0.6000          & 0.0394  & 0.7000            \\ \hline
447                & 0.0208 & 0.1000            & 0.0333 & 0.2000          & 0.0208  & 0.1000            \\ \hline
448                & 0.0000 & 0.0000            & 0.0000 & 0.0000          & 0.0000  & 0.0000            \\ \hline
449                & 0.0296 & 0.4000            & 0.0192 & 0.2000          & 0.0296  & 0.4000            \\ \hline
450                & 0.0341 & 1.0000            & 0.0208 & 0.7000          & 0.0341  & 1.0000            \\ \hline
all                & 0.0519 & 0.3600            & 0.0539 & 0.3620          & 0.0519  & 0.3600            \\ \hline \hline
\end{tabular}
\end{table*}

%\bibliographystyle{acmsiggraph}
%\nocite{*}
%\bibliography{doc}
\end{document}
