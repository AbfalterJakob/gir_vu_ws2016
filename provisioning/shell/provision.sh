#!/bin/bash
# This script will provision our VM
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install vim
sudo apt-get -y install unzip

# Install Java
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get -y update
sudo echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
sudo apt-get -y install oracle-java8-installer

# Install PHP
sudo aptitude install -y php5

# Install solr
wget 'http://www-eu.apache.org/dist/lucene/solr/6.3.0/solr-6.3.0.tgz' -P /vagrant
tar xf /vagrant/solr-6.3.0.tgz -C /vagrant
sudo rm /vagrant/solr-6.3.0.tgz

# Get solr sources
wget 'http://www-eu.apache.org/dist/lucene/solr/6.3.0/solr-6.3.0-src.tgz' -P /vagrant
tar xf /vagrant/solr-6.3.0-src.tgz -C /vagrant
sudo rm /vagrant/solr-6.3.0-src.tgz
